import { Component } from '@angular/core';
import { HttpService } from '../services/http.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  public login_sels: string;

  constructor(private http: HttpService) {

  }

  /**
   * login
   */
  public login() {
    const data = {
      email: 'hctr.kz@gmail.com',
      password: 12345
    };
    this.http.login(data).subscribe((response)  =>  {
      console.log('login', response);
      this.login_sels = 'inicio de sesion';
      
    }, (error)  =>  {
      console.log(error);
    });
  }

}
