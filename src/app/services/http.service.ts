import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpInterceptor } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  public login(data) {
    const httpPostOptions = {
      headers:
        new HttpHeaders(
          {
            'Content-Type': 'application/json'
          }),
      withCredentials: true,
    };

    return this.http.post(environment.URL.BASE + ':' + environment.URL.API.CORE_AUTH.port + environment.URL.API.CORE_AUTH.path + '/local/signin', data);
  }


}
